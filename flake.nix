{
  description = "A Brick widget for Pandoc documents.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        hpkgs = pkgs.haskell.packages.ghc8107;

        args = {
          name = "brick-widgets-pandoc";
          root = pkgs.nix-gitignore.gitignoreSource [ ] ./.;
        };
      in {
        defaultPackage = hpkgs.developPackage args;

        devShell = hpkgs.developPackage (args // {
          returnShellEnv = true;
          modifier = drv:
            pkgs.haskell.lib.addBuildTools drv (with hpkgs; [
              cabal-fmt
              cabal-install
              haskell-language-server
              nixfmt
            ]);
        });
      });
}
